package us.theappacademy.redditreader;

<<<<<<< HEAD
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.jar.JarException;
=======
import org.json.JSONObject;

import java.io.InputStream;
import java.util.InputMismatchException;
>>>>>>> b50fc461336b8ea5510727b094f4e09ef6410d4c

public class RedditPostParser {

    private static RedditPostParser parser;

<<<<<<< HEAD
    public ArrayList<RedditPost> redditPosts;

    private RedditPostParser() {
        redditPosts = new ArrayList<RedditPost>();
    }

    public static RedditPostParser getInstance() {
        if (parser == null) {
=======
    private RedditPostParser() {

    }

    public static RedditPostParser getInstance() {
        if(parser == null) {
>>>>>>> b50fc461336b8ea5510727b094f4e09ef6410d4c
            parser = new RedditPostParser();
        }
        return parser;
    }

    public JSONObject parseInputStream(InputStream inputStream) {
<<<<<<< HEAD
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        JSONObject jsonObject = null;

        String currentLine;

        try {
            while ((currentLine = streamReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
            JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
            jsonObject = new JSONObject(jsonTokener);
        } catch (IOException error) {
            Log.e("RedditPostParser", "IOException (parseInputStream):" + error);
        } catch (JSONException error) {
            Log.e("RedditPostParser", "JSONException (parseInputStream):" + error);
        }

        return jsonObject;
    }

    public void readRedditFeed(JSONObject jsonObject) {
        redditPosts.clear();
        try {
            JSONArray postData = jsonObject.getJSONObject("data").getJSONArray("children");

            for (int index = 0; index < postData.length(); index++) {
                JSONObject post = postData.getJSONObject(index).getJSONObject("data");

                String title = post.getString("title");
                String url = post.getString("url");

                RedditPost redditPost = new RedditPost(title, url);
                redditPosts.add(redditPost);
            }
        }
        catch (JSONException error) {
            Log.e("RedditPostParser", "JSONException (parseInputStream):" + error);
        }
    }
=======


    }

>>>>>>> b50fc461336b8ea5510727b094f4e09ef6410d4c
}
