package android.munozj.redditreader;



import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;



public abstract class SingleFragmentActivity extends AppCompatActivity {

    protected abstract Fragment createFragment();
    protected abstract int getLayoutResId();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());



        FragmentManager  fragmentmanager = getSupportFragmentManager();
        Fragment fragment = fragmentmanager.findFragmentById(R.id.fragment_container);

        if(fragment == null) {
            fragment = createFragment();
            fragmentmanager.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();

        }
    }
}


