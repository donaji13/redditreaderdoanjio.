
}

@Override
public void onDetach() {
        super.onDetach();
        activityCallback = null;
        }

@Nullable
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        -        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        +        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new RedditPostTask().execute(this);

        return view;
        }

public void  updateUserInterface(){
        -        RedditPostAdapter adapter = new RedditPostAdapter(ActivityCallback);
        +        RedditPostAdapter adapter = new RedditPostAdapter(activityCallback);
        recyclerView.setAdapter(adapter);
        }
        }

