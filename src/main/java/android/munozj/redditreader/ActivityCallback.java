package android.munozj.redditreader;

import android.net.Uri;

public interface ActivityCallback {

    void onPostSelected (Uri redditPostUri);
}
