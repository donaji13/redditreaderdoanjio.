import android.content.Intent;

                          package android.munozj.redditreader;

        import android.content.Intent;
import android.munozj.redditreader.RedditListFragment;
import android.munozj.redditreader.RedditWebActivity;
import android.net.Uri;
        import android.support.annotation.LayoutRes;
        import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity implements ActivityCallback{


    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_master_detail;
    }

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }


    @Override
    public void onPostSelected(Uri redditPostUri) {
        Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
        intent.setData(redditPostUri);
        startActivity(intent);

    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      